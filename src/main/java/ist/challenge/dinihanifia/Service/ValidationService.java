package ist.challenge.dinihanifia.Service;
import ist.challenge.dinihanifia.Config.Exception.*;
import ist.challenge.dinihanifia.Repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class ValidationService {

    private final UserRepository userRepository;

    public ValidationService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void fieldBlank(String username, String password) {
        if(username.isBlank() || password.isBlank()) {
            throw new FieldBlankException();
        }
    }

    public void maxUsernameLength(String value){
        if(value.length() >= 25){
            throw new LengthUsernameGreaterThanException();
        }
    }
    public void maxPasswordLength(String value){
        if(value.length() >= 25){
            throw new LengthPasswordGreaterThanException();
        }
    }
    public void existUsername(String username){
        var exist = userRepository.findByUsername(username);
        if(exist!=null){
            throw new DuplicateUsername();
        }
    }
    public void newPasswordIsSameAsOldPassword(Long id, String newPassword){
        var checkPassword = userRepository.findPasswordById(id);
        if(newPassword.equals(checkPassword.getPassword())){
            throw new DuplicatePasswordException();
        }
    }
}
