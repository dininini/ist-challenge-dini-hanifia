package ist.challenge.dinihanifia.Service;
import ist.challenge.dinihanifia.Pojo.Request.BaseRequest;
import ist.challenge.dinihanifia.Pojo.Response.BaseResponse;
import ist.challenge.dinihanifia.Pojo.Response.ListsUsersResponse;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.ResponseEntity;

public interface UserService {
    public BaseResponse registerUser(BaseRequest request, HttpServletResponse response);
    public BaseResponse loginUser(BaseRequest request, HttpServletResponse response);
    public ListsUsersResponse getAllUsers();
    public BaseResponse updateUser(Long id, BaseRequest request, HttpServletResponse response);
}
