package ist.challenge.dinihanifia.Service.Impl;
import ist.challenge.dinihanifia.Config.Constant.MessageConstant;
import ist.challenge.dinihanifia.Config.Exception.*;
import ist.challenge.dinihanifia.Pojo.Model.User;
import ist.challenge.dinihanifia.Pojo.Request.BaseRequest;
import ist.challenge.dinihanifia.Pojo.Response.BaseResponse;
import ist.challenge.dinihanifia.Pojo.Response.ListsUsersResponse;
import ist.challenge.dinihanifia.Repository.UserRepository;
import ist.challenge.dinihanifia.Service.UserService;
import ist.challenge.dinihanifia.Service.ValidationService;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ValidationService validationService;

    public UserServiceImpl(UserRepository userRepository, ValidationService validationService) {
        this.userRepository = userRepository;
        this.validationService = validationService;
    }

    @Override
    @ResponseStatus(HttpStatus.CREATED)
    public BaseResponse registerUser(BaseRequest request, HttpServletResponse response) {
        // check if field is blank
        validationService.fieldBlank(request.getUsername(), request.getPassword());
        // check username length
        validationService.maxUsernameLength(request.getUsername());
        // check password length
        validationService.maxPasswordLength(request.getPassword());
        // check if exist
        var checkUsername = userRepository.findByUsername(request.getUsername());
        if (checkUsername != null) {
            throw new DuplicateUsername();
        }
        User user = User.builder()
                .username(request.getUsername())
                .password(request.getPassword())
                .build();
        userRepository.save(user);
        response.setStatus(201);
        return BaseResponse.builder()
                .responseMessage(MessageConstant.SUCCESS)
                .build();
    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    public BaseResponse loginUser(BaseRequest request, HttpServletResponse response) {
        // check if field is blank
        validationService.fieldBlank(request.getUsername(), request.getPassword());
        // check if exist
        var existUser = userRepository.findByUsernameOrPassword(request.getUsername(), request.getPassword());
        if (existUser == null) {
            throw new NotMatchedException();
        }
        response.setStatus(200);
        return BaseResponse.builder()
                .responseMessage(MessageConstant.SUCCESS_LOGIN)
                .build();
    }

    @Override
    public ListsUsersResponse getAllUsers() {
        var checkListUsers = userRepository.findAll();
        checkListUsers.stream().filter(Objects::nonNull)
                .map(users -> {
                    if (users == null) {
                        throw new ListsUserNotFoundException();
                    }
                    return users;
                })
                .collect(Collectors.toList());
        return ListsUsersResponse.builder()
                .responseMessage(MessageConstant.SUCCESS)
                .userList(checkListUsers)
                .build();
    }

    @Override
    public BaseResponse updateUser(Long id, BaseRequest request, HttpServletResponse response) {
        // check if id is exist
        Optional<User> checkId = userRepository.findById(id);
        if (!checkId.isPresent()) {
            throw new UserIDNotFoundException(id);
        }
        // check if field is blank
        validationService.fieldBlank(request.getUsername(), request.getPassword());

        // update data user
        var updateUser = checkId.get();
        // check if username is duplicate
        validationService.existUsername(request.getUsername());
        // username
        validationService.maxUsernameLength(request.getUsername());
        // check password length
        validationService.maxPasswordLength(request.getPassword());
        // check if new password is same as old password
        validationService.newPasswordIsSameAsOldPassword(id, request.getPassword());
        updateUser.setUsername(request.getUsername());
        updateUser.setPassword(request.getPassword());
        userRepository.save(updateUser);
        response.setStatus(201);
        return BaseResponse.builder()
                .responseMessage(MessageConstant.SUCCESS)
                .build();
    }
}