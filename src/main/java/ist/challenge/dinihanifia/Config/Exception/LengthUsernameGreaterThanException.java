package ist.challenge.dinihanifia.Config.Exception;
import ist.challenge.dinihanifia.Config.Constant.MessageConstant;

public class LengthUsernameGreaterThanException extends RuntimeException{
    public LengthUsernameGreaterThanException(){
        super(MessageConstant.GREATER_MAX_LENGTH_USERNAME);
    }
}
