package ist.challenge.dinihanifia.Config.Exception;

import ist.challenge.dinihanifia.Config.Constant.MessageConstant;

public class FieldBlankException extends RuntimeException{
    public FieldBlankException(){
        super(MessageConstant.EMPTY_USERNAME_OR_PASSWORD);
    }
}
