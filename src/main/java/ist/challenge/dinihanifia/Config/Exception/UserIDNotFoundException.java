package ist.challenge.dinihanifia.Config.Exception;

import ist.challenge.dinihanifia.Config.Constant.MessageConstant;

public class UserIDNotFoundException extends RuntimeException{
    public UserIDNotFoundException(Long id){
        super(String.format(MessageConstant.ID_USER_NOT_EXIST));
    }
}
