package ist.challenge.dinihanifia.Config.Exception;

import ist.challenge.dinihanifia.Config.Constant.MessageConstant;

public class DuplicatePasswordException extends RuntimeException{
    public DuplicatePasswordException(){
        super(MessageConstant.DUPLICATE_PASSWORD);
    }
}
