package ist.challenge.dinihanifia.Config.Exception;

import ist.challenge.dinihanifia.Config.Constant.MessageConstant;

public class NotMatchedException extends RuntimeException{
    public NotMatchedException(){
        super(MessageConstant.USERNAME_OR_PASSWORD_NOT_REGISTERED);
    }
}
