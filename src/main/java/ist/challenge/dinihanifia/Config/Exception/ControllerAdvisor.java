package ist.challenge.dinihanifia.Config.Exception;
import ist.challenge.dinihanifia.Config.Constant.MessageConstant;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    // 400
    @ExceptionHandler(FieldBlankException.class)
    public ResponseEntity<Object> handleFieldBlankException(FieldBlankException ex, WebRequest webRequest){
        return new ResponseEntity<>(MessageConstant.EMPTY_USERNAME_OR_PASSWORD, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(DuplicatePasswordException.class)
    public ResponseEntity<Object> handleDuplicatePasswordException(DuplicatePasswordException ex, WebRequest webRequest){
        return new ResponseEntity<>(MessageConstant.DUPLICATE_PASSWORD, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(LengthUsernameGreaterThanException.class)
    public ResponseEntity<Object> handleMaxLengthUsernameException(LengthUsernameGreaterThanException ex, WebRequest webRequest){
        return new ResponseEntity<>(MessageConstant.GREATER_MAX_LENGTH_USERNAME, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(LengthPasswordGreaterThanException.class)
    public ResponseEntity<Object> handleMaxLengthPasswordException(LengthPasswordGreaterThanException ex, WebRequest webRequest){
        return new ResponseEntity<>(MessageConstant.GREATER_MAX_LENGTH_PASSWORD, HttpStatus.BAD_REQUEST);
    }

    // 401
    @ExceptionHandler(NotMatchedException.class)
    public ResponseEntity<Object> handleNotMatchedException(NotMatchedException ex, WebRequest webRequest){
        return new ResponseEntity<>(MessageConstant.USERNAME_OR_PASSWORD_NOT_REGISTERED, HttpStatus.UNAUTHORIZED);
    }

    // 404
    @ExceptionHandler(UserIDNotFoundException.class)
    public ResponseEntity<Object> handleUserIdNotFoundException(UserIDNotFoundException ex, WebRequest webRequest){
        return new ResponseEntity<>(MessageConstant.ID_USER_NOT_EXIST, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(ListsUserNotFoundException.class)
    public ResponseEntity<Object> handleListsException(ListsUserNotFoundException ex, WebRequest webRequest){
        return new ResponseEntity<>(MessageConstant.USERS_NOT_FOUND, HttpStatus.NOT_FOUND);
    }

    // 409
    @ExceptionHandler(DuplicateUsername.class)
    public ResponseEntity<Object> handleUsernameDuplicateException(DuplicateUsername ex, WebRequest webRequest){
        return new ResponseEntity<>(MessageConstant.EXIST_USERNAME, HttpStatus.CONFLICT);
    }
}
