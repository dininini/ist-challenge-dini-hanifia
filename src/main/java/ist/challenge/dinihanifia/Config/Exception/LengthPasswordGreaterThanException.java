package ist.challenge.dinihanifia.Config.Exception;
import ist.challenge.dinihanifia.Config.Constant.MessageConstant;

public class LengthPasswordGreaterThanException extends RuntimeException{
    public LengthPasswordGreaterThanException(){
        super(MessageConstant.GREATER_MAX_LENGTH_PASSWORD);
    }
}
