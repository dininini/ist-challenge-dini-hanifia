package ist.challenge.dinihanifia.Config.Exception;

import ist.challenge.dinihanifia.Config.Constant.MessageConstant;

public class ListsUserNotFoundException extends RuntimeException{
    public ListsUserNotFoundException(){
        super(MessageConstant.USERNAME_OR_PASSWORD_NOT_REGISTERED);
    }
}
