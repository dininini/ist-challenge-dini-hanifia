package ist.challenge.dinihanifia.Config.Exception;
import ist.challenge.dinihanifia.Config.Constant.MessageConstant;

public class DuplicateUsername extends RuntimeException{
    public DuplicateUsername(){
        super(MessageConstant.EXIST_USERNAME);
    }
}
