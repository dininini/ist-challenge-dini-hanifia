package ist.challenge.dinihanifia.Config.Constant;

public class URLConstant {
    public static final String REGISTER = "/register";
    public static final String LOGIN = "/login";
    public static final String LIST_ALL_USER = "/get-all-users";
    public static final String UPDATE = "/update/{id}";
}
