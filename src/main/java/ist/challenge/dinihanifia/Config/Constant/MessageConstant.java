package ist.challenge.dinihanifia.Config.Constant;

public class MessageConstant {
    public static final String SUCCESS_LOGIN = "Sukses Login";
    public static final String SUCCESS = "Sukses";
    public static final String EMPTY_USERNAME_OR_PASSWORD = "Username dan / atau password kosong";
    public static final String ID_USER_NOT_EXIST = "Id User %d tidak ditemukan";
    public static final String EXIST_USERNAME = "Username sudah terpakai";
    public static final String DUPLICATE_PASSWORD = "Password tidak boleh sama dengan password sebelumnya";
    public static final String GREATER_MAX_LENGTH_USERNAME = "Panjang Username tidak boleh lebih dari 25";
    public static final String GREATER_MAX_LENGTH_PASSWORD = "Panjang Password tidak boleh lebih dari 25";
    public static final String USERNAME_OR_PASSWORD_NOT_REGISTERED = "Username dan / atau password tidak terdaftar";
    public static final String USERS_NOT_FOUND = "Daftar user tidak ditemukan";
}
