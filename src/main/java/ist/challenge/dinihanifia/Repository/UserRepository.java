package ist.challenge.dinihanifia.Repository;
import ist.challenge.dinihanifia.Pojo.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findPasswordByUsername(String username);
    @Modifying
    @Query(value = "UPDATE public.tbl_user SET password = 'taylor13swift' WHERE id = 1", nativeQuery = true)
    User updatePassword(@Param("id")Long id);
    User findPasswordById(Long id);
    User findByUsernameOrPassword(String username, String password);
}
