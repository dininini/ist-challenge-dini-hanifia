package ist.challenge.dinihanifia.Pojo.Response;
import ist.challenge.dinihanifia.Pojo.Model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ListsUsersResponse {
    private String responseMessage;
    private List<User> userList;
}
