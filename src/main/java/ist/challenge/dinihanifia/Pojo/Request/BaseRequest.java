package ist.challenge.dinihanifia.Pojo.Request;

import ist.challenge.dinihanifia.Config.Constant.MessageConstant;
import ist.challenge.dinihanifia.Config.Exception.FieldBlankException;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BaseRequest {
    private String username;
    private String password;
}
