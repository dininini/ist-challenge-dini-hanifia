package ist.challenge.dinihanifia.Controller;
import ist.challenge.dinihanifia.Config.Constant.URLConstant;
import ist.challenge.dinihanifia.Pojo.Request.BaseRequest;
import ist.challenge.dinihanifia.Pojo.Response.BaseResponse;
import ist.challenge.dinihanifia.Pojo.Response.ListsUsersResponse;
import ist.challenge.dinihanifia.Service.UserService;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class MainController {
    private final UserService userService;

    @PostMapping(URLConstant.REGISTER)
    public BaseResponse registerUser(@RequestBody BaseRequest request, HttpServletResponse response){
        return userService.registerUser(request, response);
    }

    @PostMapping(URLConstant.LOGIN)
    public BaseResponse loginUser(@RequestBody BaseRequest request, HttpServletResponse response){
        return userService.loginUser(request, response);
    }
    @GetMapping(URLConstant.LIST_ALL_USER)
    public ListsUsersResponse getAllUsers(){
        return userService.getAllUsers();
    }
    @PutMapping(URLConstant.UPDATE)
    public BaseResponse updateUser(@PathVariable Long id, @RequestBody BaseRequest request, HttpServletResponse response){
        return userService.updateUser(id, request, response);
    }
}
