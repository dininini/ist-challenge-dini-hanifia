package ist.challenge.dinihanifia;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiniHanifiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiniHanifiaApplication.class, args);
	}

}
